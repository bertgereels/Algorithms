package Algorithms;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestLinkedList {
	
	LinkedList<String> LL2; 

	@BeforeEach
	void setUp() {
		LL2 = new LinkedList<String>("Hallo");
	}

	@Test
	void testGetSize() {
		LinkedList<String> LL = new LinkedList<String>();
		assertEquals(0, LL.getSize());
	}
	
	@Test
	void testGetHead() {
		assertEquals("Hallo",LL2.getHead());
	}
	
	@Test
	void testPrepend() {
		LL2.prepend("wereld");
		assertEquals("wereld", LL2.getHead());
		assertEquals(2, LL2.getSize());
	}

	@Test
	void testGetTail() {
		LL2.prepend("wereld");
		LinkedList<String> LL1 = LL2.getTail();
		assertEquals("Hallo", LL1.getHead());
		assertEquals(1, LL1.getSize());
	}
	
	@Test
	void testContains() {
		assertTrue(LL2.contains("Hallo")); //Deze string zit in LinkedList2
		assertFalse(LL2.contains("ByeBye")); //Deze string zit er niet in
	}
	
	@Test
	void testRemove() {
		LL2.prepend("wereld");
		LL2.remove("Hallo");
		assertEquals(1, LL2.getSize()); //Controleer of grootte van list 1 is na verwijderen
		assertEquals("wereld", LL2.getHead()); //Dit is het nieuwe head element
		assertEquals(0, LL2.getTail().getSize()); 
		LL2.remove("wereld");
		assertEquals(0, LL2.getSize());
	}
}
