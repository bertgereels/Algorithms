package Algorithms;

public class LinkedList<E> {
	private int size;
	private Node head;
	
	/**
	 * Constructor
	 */
	public LinkedList() {
		size = 0;
		head = null;
	}
	
	/**
	 * Constructor
	 * 	  
	 * @param element
	 */
	public LinkedList(E element) {
		size = 1;
		head = new Node(element);
	}
	
	private LinkedList(Node node) {
		head = node;
		size = count(node);
	}
	
	/**
	 * count method with loop
	 * @param node
	 * @return
	 */
	private int countL(Node node) {
		Node cursor = node;
		int count = 0;
		while(cursor != null) {
			count++;
			cursor = cursor.getNext();
		}
		return count;
	}
	
	/**
	 * count method with recursive approach
	 * @param node
	 * @return
	 */
	private int count(Node cursor, int count) {
		if(cursor == null) {
			return count;
		}
		return count(cursor.getNext(), count+1);
	}
	
	public int count(Node node) {
		return count(node, 0);
	}
	
	/**
	 * methode om te weten of de List leeg is
	 * 
	 * @return
	 */
	public boolean isEmpty() {
		return size == 0;
	}
	
	/**
	 * Methode om element te verwijderen uit de list
	 * 
	 * @param element
	 */
	public void remove(E element) {
		Node toBeRemoved;
		if(head.getElement().equals(element)) { //Als het head element het element bevat die we nodig hebben
			toBeRemoved = head;
			head = toBeRemoved.getNext();
		}
		else {
			Node before = searchNodeBefore(element, head); 
			toBeRemoved = before.getNext();
			before.setNext(toBeRemoved.getNext()); //doen vewijzen naar de node na de verwijderde node
			
		}
		toBeRemoved.setNext(null); //mem leak voorkomen
		size--; //grootte list verminderen
	}
	
	/**
	 * Private Methode om node te zoeken die voor de te verwijderen node staat
	 * Met recursie
	 * 
	 * @param element
	 * @param cursor
	 * @return
	 */
	private Node searchNodeBefore(E element, Node cursor) {
		if(cursor == null || cursor.getNext() == null) return null;
		if(cursor.getNext().getElement().equals(element)) return cursor;
		return searchNodeBefore(element, cursor.getNext());
	}
	
	/**
	 * Public contains method
	 * 
	 * @param element
	 * @return
	 */
	public boolean contains(E element) {
		return contains(head, element);
	}
	
	/**
	 * Private contains method
	 * 
	 * @param node
	 * @param element
	 * @return
	 */
	private boolean contains(Node node, E element) {
		if(node == null) return false; //Einde van de list
		if(node.getElement() == element) return true; //Het element is gevonden
		return contains(node.getNext(), element); //Met recursie volgend element bekijken
	}
	
	/**
	 * Grootte list opvragen
	 * 
	 * @return
	 */
	public int getSize() {
		return size;
	}
	
	/**
	 * Element toevoegen op het einde
	 * 
	 * @param element
	 */
	public void prepend(E element) {
		Node temp = new Node(element, head);
		head = temp;
		size++;
	}
	
	/**
	 * Head opvragen
	 * 
	 * @return
	 */
	public E getHead() {
		return head.getElement();
	}

	/**
	 * Tail opvragen
	 * 
	 * @return
	 */
	public LinkedList<E> getTail() {
		return new LinkedList<E>(head.getNext());
	}
	
	/**
	 * This is a node containing an element used in a singly linked list
	 * 
	 * @author bertg
	 *
	 * @param <E>
	 */
	private class Node {
		private E element;
		private Node next;
		
		/**
		 * Creates a node with a single element, next referring to null
		 * 
		 * @param element
		 */
		public Node(E element) { 				//1e constructor
			this(element,null);					//Om de eerste node aan te maken
		}
		
		/**
		 * Created a node with a single element, next referring to next element
		 * 
		 * @param element
		 * @param next
		 */
		public Node(E element, Node next) {	//2e constructor
			this.element = element;
			this.next = next;
		}
		
		public E getElement() { 				//E is return type
			return element;
		}
		
		/**
		 * Getter for next node
		 *
		 * @return next
		 */
		public Node getNext(){				//Node<E> is return type
			return next;
		}
		
		/**
		 * Setter for next node
		 * 
		 * @param next
		 */
		public void setNext(Node next) {
			this.next = next;
		}
	}


}


